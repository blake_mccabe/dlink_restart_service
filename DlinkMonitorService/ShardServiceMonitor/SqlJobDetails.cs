﻿using System;

namespace DlinkMonitor
{
    /// <summary>
    /// Sql Job details model
    /// </summary>
    public class SqlJobDetails
    {
        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the scheduled times.
        /// </summary>
        /// <value>
        /// The scheduled times.
        /// </value>
        public string ScheduledTimes { get; set; }


        /// <summary>
        /// Gets or sets the LastRuntime.
        /// </summary>
        /// <value>
        /// The LastRuntime.
        /// </value>
        public DateTime LastRuntime { get; set; }
    }
}
