﻿using System;
using System.Configuration;
using System.Net.Mail;

namespace DlinkMonitor
{
    public class Email
    {
        private string Environment = ConfigurationManager.AppSettings["Environment"].ToString();

        private bool isMailEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsMailEnabled"]);

        private string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();

        private string SendEmailAddress = ConfigurationManager.AppSettings["SendEmailAddress"].ToString();

        private string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();

        private string[] toEmailList = ConfigurationManager.AppSettings["ToEmailAddress"].Split(',');

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="body">The body.</param>
        public void SendMail(string serviceName, string body)
        {
            try
            {
                if (isMailEnabled)
                {
                    MailMessage mail = new MailMessage();

                    SmtpClient SmtpServer = new SmtpClient(SMTPServer);

                    mail.From = new MailAddress(SendEmailAddress);
                    foreach (var emailId in toEmailList)
                    {
                        mail.To.Add(emailId);
                    }
                    mail.Subject = EmailSubject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;

                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                }
                else
                {
                    Logger.Info("Emailing facility is not enabled to Send mails.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                Logger.Error("InnerException : " + ex.InnerException.Message);
            }
            
        }

        /// <summary>
        /// Creates the email body for service start.
        /// </summary>
        /// <returns></returns>
        public string CreateEmailBodyForServiceStart(string serviceName)
        {
            var body = @"<!DOCTYPE html> <html>     <body>         <div>             <p>The "+serviceName+" service has been restarted in " + Environment + " at <span style = 'color:#0000FF'>{DateTime}</span> after it got crashed.<p> <p>Please connect to " + Environment + " server to troubleshoot further.</p>             </ div >         </ body >     </ html > ";

            body = body.Replace("{DateTime}", DateTime.Now.ToString());

            return body;
        }
    }
}
