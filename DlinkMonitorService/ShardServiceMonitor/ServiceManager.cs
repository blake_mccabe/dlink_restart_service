﻿using System;
using System.ServiceProcess;

namespace DlinkMonitor
{
    /// <summary>
    /// Service manager class.
    /// </summary>
    public static class ServiceManager
    {
        /// <summary>
        /// Checks the service status.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns></returns>
        public static ServiceControllerStatus CheckServiceStatus(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            return service.Status;
        }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        public static void StartService(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            try
            {
                service.Start();

                service.WaitForStatus(ServiceControllerStatus.Running);

                Logger.Info(service.ServiceName + " service started successfully");
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                Logger.Error("InnerException : " + ex.InnerException.Message);
            }
        }

        public static void WaitForServiceStart(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            try
            {
                service.WaitForStatus(ServiceControllerStatus.Running);
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                Logger.Error("InnerException : " + ex.InnerException.Message);
            }
        }

        public static void WaitForServiceStop(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            try
            {
                service.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                Logger.Error("InnerException : " + ex.InnerException.Message);
            }
        }
    }
}
