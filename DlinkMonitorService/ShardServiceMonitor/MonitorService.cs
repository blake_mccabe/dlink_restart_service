﻿using System;
using System.ServiceProcess;
using Quartz;
using Quartz.Impl;
using System.Configuration;

namespace DlinkMonitor
{
    /// <summary>
    /// Starts / stops the windows services during Sql Job run
    /// </summary>
    /// <seealso cref="System.ServiceProcess.ServiceBase" />
   
    partial class MonitorService : ServiceBase
    {
        /// <summary>
        /// The cron expression
        /// </summary>
        private string cronExpression = Convert.ToString(ConfigurationManager.AppSettings["CronExpression"]);

        public MonitorService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            Process();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            Logger.Info(this.ServiceName + " Service has stopped");
        }

        public void Process()
        {
            //throw new NotImplementedException();

            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

            scheduler.Start();

            IJobDetail job = JobBuilder.Create<JobScheduler>()
                .WithIdentity("monitorJob", "monitorGroup")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger", "monitorGroup")
                .WithCronSchedule(cronExpression)
                .ForJob("monitorJob", "monitorGroup")
                .Build();

            scheduler.ScheduleJob(job, trigger);

            Logger.Info(this.ServiceName + " Service has started");
        }
    }
}
