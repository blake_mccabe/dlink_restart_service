﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;


namespace DlinkMonitor
{
    class NetworkInfo
    {
        // This version loops through the available interfaces to try to find an active default gateway
        public static IPAddress GetDefaultGateway()
        {
            return NetworkInterface
                .GetAllNetworkInterfaces()
                .Where(n => n.OperationalStatus == OperationalStatus.Up)
                .Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .SelectMany(n => n.GetIPProperties()?.GatewayAddresses)
                .Select(g => g?.Address)
                // .Where(a => a.AddressFamily == AddressFamily.InterNetwork)
                // .Where(a => Array.FindIndex(a.GetAddressBytes(), b => b != 0) >= 0)
                .FirstOrDefault(a => a != null);
        }

        [DllImport("iphlpapi.dll", CharSet = CharSet.Auto)]
        private static extern int GetBestInterface(UInt32 destAddr, out UInt32 bestIfIndex);

        // This version tries to find the default gateway of the NIC actually used to route to a defined address
        public static IPAddress GetGatewayForDestination(IPAddress destinationAddress)
        {
            uint destAddr = BitConverter.ToUInt32(destinationAddress.GetAddressBytes(), 0);

            uint interfaceIndex;
            int result = GetBestInterface(destAddr, out interfaceIndex);
            if (result != 0)
                throw new Win32Exception(result);

            foreach (var ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                var niprops = ni.GetIPProperties();
                if (niprops == null)
                    continue;

                var gateway = niprops.GatewayAddresses?.FirstOrDefault()?.Address;
                if (gateway == null)
                    continue;

                if (ni.Supports(NetworkInterfaceComponent.IPv4))
                {
                    var v4Props = niprops.GetIPv4Properties();
                    if (v4Props == null)
                        continue;

                    if (v4Props.Index == interfaceIndex)
                        return gateway;
                }

                // GWs should have IPv6 disabled - may not be necessary
                if (ni.Supports(NetworkInterfaceComponent.IPv6))
                {
                    var v6Props = niprops.GetIPv6Properties();
                    if (v6Props == null)
                        continue;

                    if (v6Props.Index == interfaceIndex)
                        return gateway;
                }
            }

            return null;
        }
    }
}
