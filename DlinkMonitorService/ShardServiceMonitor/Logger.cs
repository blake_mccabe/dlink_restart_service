﻿namespace DlinkMonitor
{
    /// <summary>
    /// Logger utility class to make use of log4net
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        private static log4net.ILog Log { get; set; }

        /// <summary>
        /// Initializes the <see cref="Logger"/> class.
        /// </summary>
        static Logger()
        {
            Log = log4net.LogManager.GetLogger(typeof(Logger));
        }

        /// <summary>
        /// Errors the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Error(object msg)
        {
            Log.Error(msg);
        }

        /// <summary>
        /// Informations the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Info(object msg)
        {
            Log.Info(msg);
        }
    }
}
