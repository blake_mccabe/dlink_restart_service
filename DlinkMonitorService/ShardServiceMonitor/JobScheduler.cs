﻿using System;
using System.Collections.Generic;
using System.Linq;
using Quartz;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.NetworkInformation;
using System.Globalization;
using System.ServiceProcess;

namespace DlinkMonitor
{
    /// <summary>
    /// Job scheduler class
    /// </summary>
    /// <seealso cref="Quartz.IJob" />
    [DisallowConcurrentExecution]
    
    public class JobScheduler : IJob
    {
        // existing app settings values - keep for now as we may reuse
        private static string Environment = ConfigurationManager.AppSettings["Environment"].ToString();
        private static double timeBeforeCheck = Convert.ToDouble("-" + ConfigurationManager.AppSettings["TimeBeforeCheck"]);
        private static double timeAfterCheck = Convert.ToDouble(ConfigurationManager.AppSettings["TimeAfterCheck"]);
        private static List<string> processingServices = ConfigurationManager.AppSettings["ProcessingService"].ToString().Split(',').ToList();
        private static string sqlJobMonitor = ConfigurationManager.AppSettings["SqlJobMonitorService"].ToString();
        //private List<SqlJobDetails> JobsList = GetJobsListFromTable();  // Keep the old SQL version around for now.
        private List<JobDetails> JobsList = GetJobsList();  
        private bool iSDbMaintenanceTime = false;

        // new app settings values
        private static string TargetAddressOne = ConfigurationManager.AppSettings["TargetAddressOne"].ToString();
        private static string TargetAddressTwo = ConfigurationManager.AppSettings["TargetAddressTwo"].ToString();
        private static int TargetAddressAttemptCount = Convert.ToInt32(ConfigurationManager.AppSettings["TargetAddressAttemptCount"].ToString());
        private static int TargetAddressFailedAttemptCount = Convert.ToInt32(ConfigurationManager.AppSettings["TargetAddressFailedAttemptCount"].ToString());
        private static int MinutesSinceLastDlinkReboot = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesSinceLastDlinkReboot"].ToString());
        private static int MinutesSinceLastGatewayReboot = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesSinceLastGatewayReboot"].ToString());

        /// <summary>
        /// Called by the <see cref="T:Quartz.IScheduler" /> when a <see cref="T:Quartz.ITrigger" />
        /// fires that is associated with the <see cref="T:Quartz.IJob" />.
        /// </summary>
        /// <param name="context">The execution context.</param>
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Logger.Info("Entering health check loop.");

                foreach (var job in JobsList)
                {
                    // Check for connectivity and reboot the DLink if in the time window
                    if (job.JobName == "DLink")
                    {
                        // run connectivity checks
                        Logger.Info("Attempting connectivity checks.");

                        Ping pingSender1 = new Ping();
                        PingReply reply1 = pingSender1.Send(TargetAddressOne, 10000);

                        Ping pingSender2 = new Ping();
                        PingReply reply2 = pingSender2.Send(TargetAddressTwo, 10000);

                        if (reply1 != null && reply1.Status == IPStatus.Success)
                        {
                            Logger.Info("Target address one pinged successfully.");
                        }
                        else if (reply2 != null && reply2.Status == IPStatus.Success)
                        {
                            Logger.Info("Target address two pinged successfully.");
                        }
                        else
                        {
                            Logger.Error("Target address pings unsuccessful. Checking interval since last router reboot attempt.");

                            // TODO Add reboot counter and limiter logic
                            // For now just try the reboot

                            // identify the default gateway and send reboot command
                            try
                            {
                                IPAddress targetAddress = IPAddress.Parse(TargetAddressOne);
                                IPAddress defaultGateway = NetworkInfo.GetGatewayForDestination(targetAddress);

                                // create a new pseudo telnet connection to DLink on port "23"
                                TelnetConnection tc = new TelnetConnection(defaultGateway.ToString(), 23);

                                // login with user "admin",password "wayneix" for production
                                // do NOT include this info in a human readable config file
                                // using a timeout of 1000ms, and show server output
                                string s = tc.Login("admin", "admin", 1000);
                                Console.Write(s);

                                // check if connection is successful
                                // server output should end with "$" or ">", otherwise the connection failed
                                // added "." for DLink
                                string prompt = s.TrimEnd();
                                prompt = s.Substring(prompt.Length - 1, 1);
                                if (prompt != "$" && prompt != ">" && prompt != ".")
                                    throw new Exception("Connection failed");

                                // if connected, send reboot command
                                if (tc.IsConnected)
                                {
                                    Logger.Info("Router connection established, sending reboot command.");
                                    tc.WriteLine(".reboot");
                                    Logger.Info("Reboot command sent.");
                                }
                                
                            }
                            catch (Exception e)
                            {
                                Logger.Error("***** Attempt to find default gateway and send reboot command failed. *****");
                                Logger.Error("Message:" + e.Message);
                                Logger.Error("StackTrace:" + e.StackTrace);
                                if (e.InnerException != null) Logger.Error("InnerException : " + e.InnerException.Message);
                            }

                        }



                        // if connectivity check fails and outside of time window, send reboot


                        // alternatively, reboot the GW box - TBD if this is in scope
                    }
                }

                /* preserve maintenance window checks for now if we want to reuse them later
                if (!iSDbMaintenanceTime)
                {
                    //check if the Sql Job Monitor service is stopped. If stopped start the same
                    if (ServiceManager.CheckServiceStatus(sqlJobMonitor) == ServiceControllerStatus.Stopped
                    || ServiceManager.CheckServiceStatus(sqlJobMonitor) == ServiceControllerStatus.Paused)
                    {
                        ServiceManager.StartService(sqlJobMonitor);

                        var mail = new Email();
                        var body = mail.CreateEmailBodyForServiceStart(sqlJobMonitor);
                        mail.SendMail(sqlJobMonitor, body);
                    }


                    //check if the processingService service is stopped. If stopped start the same
                    foreach (var service in processingServices)
                    {
                        if (ServiceManager.CheckServiceStatus(service) == ServiceControllerStatus.Stopped
                    || ServiceManager.CheckServiceStatus(service) == ServiceControllerStatus.Paused)
                        {
                            ServiceManager.StartService(service);

                            var mail = new Email();
                            var body = mail.CreateEmailBodyForServiceStart(service);
                            mail.SendMail(service, body);
                        }
                    }
                }
                */
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                if (ex.InnerException != null) Logger.Error("InnerException : " + ex.InnerException.Message);
            }
        }

        private static List<JobDetails> GetJobsList()
        {
            var jobsList = new List<JobDetails>();

            try
            {
                var jobDetail = new JobDetails {JobName = "DLink", JobDescription = "DLink AdditionalInfo"};
                jobsList.Add(jobDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                if (ex.InnerException != null) Logger.Error("InnerException : " + ex.InnerException.Message);
            }

            return jobsList;
        }



        /// <summary>
        /// Gets the job times from table.
        /// </summary>
        /// <returns></returns>
        /// preserve for now if we want to reuse from the local GW database 
        private static List<SqlJobDetails> GetJobsListFromTable()
        {
            var timeList = new List<string>();
            var jobsList = new List<SqlJobDetails>();

            var constr = ConfigurationManager.AppSettings["ConnectionString"];

            var query = string.Format("Select JobName, ScheduledTime, ISNULL(LastRuntime,DATEADD(hour,-1,GETDATE())) AS LastRuntime from JobScheduleTime {0}", ConfigurationManager.AppSettings["GetAllJobTimes"] != null ? "WHERE StopService = " + Convert.ToString(ConfigurationManager.AppSettings["GetAllJobTimes"]) : string.Empty);

            try
            {
                using (var con = new SqlConnection(constr))
                {
                    con.Open();

                    var cmd = new SqlCommand(query, con);

                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        var scheduledTimes = dr["ScheduledTime"].ToString();
                        var jobdetail = new SqlJobDetails { JobName = dr["JobName"].ToString(), ScheduledTimes = dr["ScheduledTime"].ToString(), LastRuntime = DateTime.Parse(dr["LastRuntime"].ToString()) };
                        jobsList.Add(jobdetail);
                        timeList.Add(scheduledTimes);
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Message:" + ex.Message);
                Logger.Error("StackTrace:" + ex.StackTrace);
                if (ex.InnerException != null) Logger.Error("InnerException : " + ex.InnerException.Message);
            }

            return jobsList;
        }
    }
}
