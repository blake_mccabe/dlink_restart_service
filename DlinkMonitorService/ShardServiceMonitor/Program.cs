﻿using System.ServiceProcess;

namespace DlinkMonitor
{
    /// <summary>
    /// Main program
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            #if(DEBUG)

            MonitorService monSvc = new MonitorService();
            monSvc.Process();


            #else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MonitorService()
            };
            ServiceBase.Run(ServicesToRun);

            //var js = new JobScheduler();

            //js.Execute(null);
            #endif
        }
    }
}
