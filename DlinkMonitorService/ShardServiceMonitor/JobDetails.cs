﻿namespace DlinkMonitor
{
    class JobDetails
    {
        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the additional info about the Job.
        /// </summary>
        /// <value>
        /// Additional Info about the Job.
        /// </value>
        public string JobDescription { get; set; }
    }
}
